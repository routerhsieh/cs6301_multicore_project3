package search;

public class CoarseGrainedBinarySearchTree implements BinarySearchTree<Integer> {
	//TODO: Can we still use generic parameter when implementing an interface with generic parameter? 
	private Node<Integer> root;
	private int maxSize = 0;
	private int currSize = 0;
	
	public CoarseGrainedBinarySearchTree (int maxSize) {
		this.root = null;
		this.maxSize = maxSize;
	}
	
	private boolean isEmpty() {
		return currSize == 0;
	}

	@Override
	public boolean search(Integer toSearch) {
		synchronized (this) {
			if (isEmpty()) {
				return false;
			}
			else {
				Node<Integer> curr = root;
				while (!curr.isExternal()) {
					if (toSearch >= curr.getData()) {
						curr = curr.getRight();
					}
					else {
						curr = curr.getLeft();
					}
				}
				return curr.getData() == toSearch;
			}
		}
	}

	@Override
	public boolean insert(Integer toInsert) {
		synchronized (this) {
			if (!isEmpty()) {
				if (currSize == maxSize) {
					return false;
				}
				
				Node<Integer> curr = root, parent = null;
				while (!curr.isExternal()) {
					parent = curr;
					if (toInsert.compareTo(curr.getData()) >= 0) {
						curr = curr.getRight();
					}
					else {
						curr = curr.getLeft();
					}
				}
				
				if (toInsert.compareTo(curr.getData()) == 0) {
					//Duplicated insertation
					return false;
				}
				else {
					Node<Integer> newHead, newLeft, newRight;
					if (toInsert.compareTo(curr.getData()) > 0) {
						newRight = new Node<Integer>(toInsert.intValue());
						newLeft = curr;
						newHead = new Node<Integer>(toInsert.intValue(),
								newLeft, newRight);
					} else {
						newRight = curr;
						newLeft = new Node<Integer>(toInsert.intValue());
						newHead = new Node<Integer>(curr.getData().intValue(),
								newLeft, newRight);
					}

					if (parent != null) {
						// Need to find out the new subtree should be parent's
						// left or right child
						if (curr == parent.getLeft()) {
							parent.setLeft(newHead);
						} else {
							parent.setRight(newHead);
						}
					} else {
						// that means we only have one single node in BST
						this.root = newHead;
					}
					currSize += 1;
					return true;
				}
			}
			else {
				//Create new BST with single external node as root
				this.root = new Node<Integer>(toInsert);
				currSize += 1;
				return true;
			}
		}
	}

	@Override
	public boolean remove(Integer toRemove) {
		synchronized (this) {
			if (!isEmpty()) {
				Node<Integer> curr = root, parent = null, grandParent = null;
				while (!curr.isExternal()) {
					grandParent = parent;
					parent = curr;
					if (toRemove.compareTo(curr.getData()) >= 0) {
						curr = curr.getRight();
					}
					else {
						curr = curr.getLeft();
					}
				}
				
				if (toRemove.compareTo(curr.getData()) != 0) {
					//Can't find the target we want to remove
					return false;
				}
				else {
					
					if (parent == null) {
						this.root = null;
						currSize -= 1;
						return true;
					}
					else {

						Node<Integer> sibiling;
						if (curr == parent.getLeft()) {
							sibiling = parent.getRight();
						} else {
							sibiling = parent.getLeft();
						}

						if (grandParent == null) {
							this.root = sibiling;
						} else {
							if (parent == grandParent.getLeft()) {
								grandParent.setLeft(sibiling);
							} else {
								grandParent.setRight(sibiling);
							}
						}
						currSize -= 1;
						return true;
					}
				}
			}
			else {
				//This tree is empty, so there is nothing to remove
				return false;
			}
		}
	}

	@Override
	public Node<Integer> getRoot() {
		return this.root;
	}
}
