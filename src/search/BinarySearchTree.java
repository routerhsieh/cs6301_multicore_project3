package search;

public interface BinarySearchTree<T> {
	Node<T> getRoot();
	boolean search(T toSearch);
	boolean insert(T toInsert);
	boolean remove(T toRemove);
}
