package search;

public class TestAll {
	public static void main(String args[]) {
		String operationType, treeType;
		int keySpaceRange, threadNums, runs = 1;
		
		if (args.length < 4) {
			System.out.printf("Error: Please enter tree type, key space range, operation type and number of threads!!\n");
			System.out.println("Tree type: coarse or lazy");
			System.out.println("Key space range: non-negative integer");
			System.out.println("Operation type: read, mix or write");
			System.out.println("Number of threads: non-negative integer");
			return;
		}
		
		treeType = args[0];
		keySpaceRange = Integer.parseInt(args[1]);
		operationType = args[2];
		threadNums = Integer.parseInt(args[3]);
		
		if (args.length > 4) {
			runs = Integer.parseInt(args[4]);
		}
		
		

		//ConcurrentSearchTreeTest treeTest = new ConcurrentSearchTreeTest(tree, operationType, keySpaceRange, threadNums);
		for (int idx = 0; idx < runs; idx++) {
			
			BinarySearchTree<Integer> tree;
			switch(treeType) {
			case "coarse":
				tree = new CoarseGrainedBinarySearchTree(keySpaceRange);
				break;
			case "lazy":
				tree = new LazySynchronizedBinarySearchTree(keySpaceRange);
				break;
			default:
				System.out.printf("Error: Unknown tree type %s!!\n", treeType);
				return;
			}
			
			ConcurrentSearchTreeTest treeTest = new ConcurrentSearchTreeTest(tree, operationType, keySpaceRange, threadNums);
			treeTest.test();
			if (treeTest.verifyTree()) {
				System.out.printf("OperationType: %s\n", operationType);
				System.out.printf("KeySpaceRange: %d\n", keySpaceRange);
				System.out.printf("ThreadNumber: %d\n", threadNums);
				System.out.printf("Throughput: %f\n", treeTest.getThroughput());
				System.out.printf("\n");
			}
			else {
				System.out.println("Tree Verify failed!!");
				return;
			}
		}
	}
}
