package search;

import java.util.*;

public class TestWorker implements Runnable {
	private int searchNums;
	private int insertNums;
	private int removeNums;
	private int keyRange;
	private BinarySearchTree<Integer> tree;
	private Random random = new Random(System.nanoTime());
	private ArrayList<WorkItem> workList;
	
	public TestWorker (int[] operationNumbers, int keyRange, BinarySearchTree<Integer> tree) {
		this.searchNums = operationNumbers[0];
		this.insertNums = operationNumbers[1];
		this.removeNums = operationNumbers[2];
		this.keyRange = keyRange;
		this.tree = tree;
		initialize();
	}
	
	private enum ActionType {
		SEARCH,
		INSERT,
		REMOVE;
	}
	
	private class WorkItem {
		public ActionType action;
		public int value;
		
		public WorkItem (ActionType action, int value) {
			this.action = action;
			this.value = value;
		}
	}
	
	private void initialize() {
		this.workList = new ArrayList<WorkItem>(searchNums + insertNums + removeNums);
		
		for (int idx = 1; idx <= searchNums; idx++) {
			WorkItem work = new WorkItem(ActionType.SEARCH, random.nextInt(keyRange) + 1);
			workList.add(work);
		}
		
		for (int idx = 1; idx <= insertNums; idx++) {
			WorkItem work = new WorkItem(ActionType.INSERT, random.nextInt(keyRange) + 1);
			workList.add(work);
		}
		
		for (int idx = 1; idx <= removeNums; idx++) {
			WorkItem work = new WorkItem(ActionType.REMOVE, random.nextInt(keyRange) + 1);
			workList.add(work);
		}
		
		Collections.shuffle(workList, new Random(System.nanoTime()));
	}
	
	@Override
	public void run() {
		for (WorkItem work: workList) {
			switch (work.action) {
			case SEARCH:
				tree.search(work.value);
				break;
			case INSERT:
				tree.insert(work.value);
				break;
			case REMOVE:
				tree.remove(work.value);
				break;
			default:
				break;
			}
		}
	}

}
