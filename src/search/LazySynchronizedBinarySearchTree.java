package search;

import java.util.concurrent.atomic.*;

public class LazySynchronizedBinarySearchTree implements BinarySearchTree<Integer> {
	private AtomicReference<Node<Integer>> rootReference = new AtomicReference<Node<Integer>>();
	private int maxSize;
	private AtomicInteger currSize = new AtomicInteger(0);
	
	public LazySynchronizedBinarySearchTree (int maxSize) {
		this.maxSize = maxSize;
	}
	
	private boolean isEmpty() {
		return currSize.get() == 0;
	}
	
	@Override
	public Node<Integer> getRoot() {
		return rootReference.get();
	}

	@Override
	public boolean search(Integer toSearch) {
		if (isEmpty()) {
			return false;
		}
		else {
			Node<Integer> curr = rootReference.get();
			while (curr != null && !curr.isExternal()) {
				if (toSearch.compareTo(curr.getData()) >= 0) {
					curr = curr.getRight();
				}
				else {
					curr = curr.getLeft();
				}
			}
			
			if (curr == null) {
				//It means that before starting search, 
				//the BST is been cleaned, so the root point to null;
				return false;
			}
			else {
				return (toSearch.compareTo(curr.getData()) == 0) && !curr.isMarked();
			}
		}
	}

	@Override
	public boolean insert(Integer toInsert) {
		Node<Integer> toInsertNode = new Node<Integer>(toInsert.intValue());
		insertLoop: while (true) {
			if (rootReference.compareAndSet(null, toInsertNode)) {
				currSize.incrementAndGet();
				return true;
			} else {
				if (currSize.get() == maxSize) {
					// The tree is full!!
					return false;
				}

				Node<Integer> curr = rootReference.get(), parent = null;
				while (curr != null && !curr.isExternal()) {
					if (curr.isMarked()) {
						// When traversal from root, if there is a node on our
						// path is marked,
						// then we need to restart from begining
						continue insertLoop;
					} else {
						parent = curr;
						if (toInsert.compareTo(curr.getData()) >= 0) {
							curr = curr.getRight();
						} else {
							curr = curr.getLeft();
						}
					}
				}// end of while

				if (curr == null) {
					// It means that before starting search,
					// the BST is been cleaned, so the root point to null;
					continue insertLoop;
				}

				curr.lock();
				try {
					if (toInsert.compareTo(curr.getData()) == 0) {
						if (curr.isMarked()) {
							// The curr is duplicated data, but it has been
							// marked to remove.
							continue insertLoop;
						} else {
							return false;
						}
					}

					Node<Integer> newHead, newLeft, newRight;
					if (toInsert.compareTo(curr.getData()) > 0) {
						newRight = toInsertNode;
						newLeft = curr;
						newHead = new Node<Integer>(toInsert.intValue(),
								newLeft, newRight);
					} else {
						newRight = curr;
						newLeft = toInsertNode;
						newHead = new Node<Integer>(curr.getData().intValue(),
								newLeft, newRight);
					}

					if (parent != null) {
						parent.lock();
						try {
							// Need to find out the curr is the left or right
							// child of parent
							if (parent.isMarked()) {
								continue insertLoop;
							}

							if (curr == parent.getLeft()) {
								parent.setLeft(newHead);
							} else if (curr == parent.getRight()) {
								parent.setRight(newHead);
							} else {
								// If we go to here, there might be some bad
								// happen.
								// Maybe we should through out an exception?
								return false;
							}
							currSize.incrementAndGet();
							return true;
						} finally {
							parent.unlock();
						}
					} else {
						// This case means that we only have one external node
						// as root in BST
						if (rootReference.compareAndSet(curr, newHead)) {
							currSize.incrementAndGet();
							return true;
						} else {
							// Not sure if it is necessary,
							// because the curr is point to the only node(root)
							// in BST,
							// so rootReference should equal to curr....
							continue insertLoop;
						}
					}
				} finally {
					curr.unlock();
				}
			}
		}// end of routineLoop
	}

	@Override
	public boolean remove(Integer toRemove) {
		removeLoop: while (true) {
			if (!isEmpty()) {
				Node<Integer> curr = rootReference.get(), parent = null, grandParent = null;
				while (curr != null && !curr.isExternal()) {
					if (curr.isMarked()) {
						continue removeLoop;
					}
					else {
						grandParent = parent;
						parent = curr;
						if (toRemove.compareTo(curr.getData()) >= 0) {
							curr = curr.getRight();
						} else {
							curr = curr.getLeft();
						}
					}
				}
				
				if (curr == null) {
					// It means that before starting search,
					// the BST is been cleaned, so the root point to null;
					// In this case, the tree is empty, so there is nothing to remove.
					return false;
				}
				
				curr.lock();
				try {
					if (toRemove.compareTo(curr.getData()) != 0 || curr.isMarked()) {
						//Didn't find the target we want to remove, 
						//or it has been removed by other thread
						return false;
					}
					else {
						if (curr.isMarked()) {
							return false;
						}
						if (parent != null) {
							parent.lock();
							try {
								if (parent.isMarked()) {
									continue removeLoop;
								}
								
								Node<Integer> sibiling;
								if (curr == parent.getLeft()) {
									sibiling = parent.getRight();
								}
								else if (curr == parent.getRight()){
									sibiling = parent.getLeft();
								}
								else {
									//This should not happen, right now we just start over.
									continue removeLoop;
								}
								
								if (grandParent != null) {
									grandParent.lock();
									try {
										if (grandParent.isMarked()) {
											continue removeLoop;
										}
										
										//Need to know parent is left or right child of grandParent
										if (parent == grandParent.getLeft()) {
											parent.setMarked();
											curr.setMarked();
											currSize.decrementAndGet();
											grandParent.setLeft(sibiling);
											return true;
										}
										else if (parent == grandParent.getRight()) {
											parent.setMarked();
											curr.setMarked();
											currSize.decrementAndGet();
											grandParent.setRight(sibiling);
											return true;
										}
										else {
											//Should not goto here
											continue removeLoop;
										}
									}
									finally {
										grandParent.unlock();
									}
								}
								else {
									//The BST only has one root and two child, 
									//and we are going to remove one of the child, 
									//the rest child should become the new root of BST
									if (rootReference.compareAndSet(parent, sibiling)) {
										//TODO: Should we mark parent and curr before changing the rootReference?
										parent.setMarked();
										curr.setMarked();
										//Change the root pointer to sibiling, 
										//the old root and remove target will be recycle later by GC
										currSize.decrementAndGet();
										return true;
									}
									else {
										//Because the BST only have one root, 
										//it should be the parent.
										continue removeLoop;
									}
								}
							}
							finally {
								parent.unlock();
							}
						}
						else {
							//We are removing the only node in BST
							if (curr.setMarked()) {
								rootReference.set(null);
								currSize.decrementAndGet();
								return true;
							}
							else {
								//The only root had been marked by other thread, 
								//so we just return false
								return false;
							}
						}
					}
				}
				finally {
					curr.unlock();
				}
			} else {
				//Tree is empty, there is nothing to remove
				return false;
			}
		}
	}

}
