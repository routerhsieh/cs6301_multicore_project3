package search;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.*;

public class Node<T> {
	private T data;
	private boolean isExternal;
	private volatile Node<T> left, right;
	/* These two members only used in lazy synchronized version */
	private AtomicBoolean isMarked = new AtomicBoolean(false);
	private Lock nodeLock = new ReentrantLock();
	
	public Node() {
		this.setExternal(false);
	}
	
	public Node(T data) {
		this.setData(data);
		this.setExternal(true);
		this.setLeft(null);
		this.setRight(null);
	}
	
	public Node(T data, Node<T> left, Node<T> right) {
		this.setData(data);
		this.setExternal(false);
		this.setLeft(left);
		this.setRight(right);
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public boolean isExternal() {
		return isExternal;
	}

	public void setExternal(boolean isExternal) {
		this.isExternal = isExternal;
	}

	public Node<T> getRight() {
		return right;
	}

	public void setRight(Node<T> right) {
		this.right = right;
	}

	public Node<T> getLeft() {
		return left;
	}

	public void setLeft(Node<T> left) {
		this.left = left;
	}

	public boolean isMarked() {
		return isMarked.get();
	}

	public boolean setMarked() {
		return isMarked.compareAndSet(false, true);
	}
	
	public void lock() {
		nodeLock.lock();
	}
	
	public void unlock() {
		nodeLock.unlock();
	}
}
