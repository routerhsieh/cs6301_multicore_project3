package search;

import java.util.*;

public class ConcurrentSearchTreeTest {
	private BinarySearchTree<Integer> testTree = null;
	private int keySpaceRange = 0;
	private double[] ratios; //ratio order: search, insert, remove
	private int[] operationNumbers;
	private long totalTimes;
	private long totalOperations;
	private int threadNumber = Runtime.getRuntime().availableProcessors();
	private Thread[] threads;
	
	public ConcurrentSearchTreeTest (BinarySearchTree<Integer> testTree, String operationType, int keySpaceRange,int threadNumber) {
		this.testTree = testTree;
		this.keySpaceRange = keySpaceRange;
		this.threadNumber = threadNumber;
		this.threads = new Thread[threadNumber];
		
		switch (operationType) {
		case "read":
			ratios = new double[] {0.9, 0.09, 0.01};
			break;
		case "mix":
			ratios = new double[] {0.7, 0.2, 0.1};
			break;
		case "write":
			ratios = new double[] {0.0, 0.5, 0.5};
			break;
		default:
			break;
		}
	}
	
	private void initialize() {
		double searchRatio = ratios[0], insertRatio = ratios[1], removeRatio = ratios[2];
		int totalInserts = keySpaceRange;
		int totalSearchs = (int)(totalInserts / insertRatio * searchRatio);
		int totalRemoves = (int)(totalInserts / insertRatio * removeRatio);
		
		totalOperations = totalInserts + totalSearchs + totalRemoves;
		operationNumbers = new int[] {totalInserts / threadNumber, totalSearchs / threadNumber, totalRemoves / threadNumber};
	}
	
	private void treeTraversal(Node<Integer> curr, List<Integer> resultList) {
		if (curr.isExternal()) {
			resultList.add(curr.getData());
		}
		else {
			if (curr.getLeft() != null && curr.getRight() != null) {
				treeTraversal(curr.getLeft(), resultList);
				treeTraversal(curr.getRight(), resultList);
			}
			else {
				System.out.printf("Error: internal node should always have two childs!!\n");
				return;
			}
		}
	}
	
	public boolean verifyTree() {
		LinkedList<Integer> resultList = new LinkedList<Integer>();
		Node<Integer> root = testTree.getRoot();
		if (root == null) {
			return true;
		}
		treeTraversal(testTree.getRoot(), resultList);
		
		int[] resultArray = new int[resultList.size()];
		int idx = 0;
		for (Integer e : resultList) {
			resultArray[idx++] = e.intValue();
		}
		
		for (idx = 1; idx < resultList.size(); idx++) {
			if (resultArray[idx] < resultArray[idx - 1]) {
				return false;
			}
		}
		return true;
	}
	
	public double getThroughput() {
		return  (double)this.totalTimes / (double)this.totalOperations;
	}
	
	public void test() {
		initialize();
		
		for (int idx = 0; idx < threadNumber; idx++) {
			threads[idx] = new Thread(new TestWorker(operationNumbers, keySpaceRange, testTree));
		}
		
		long startTime = System.nanoTime();
		for (int idx = 0; idx < threadNumber; idx++) {
			threads[idx].start();
		}
		
		for (int idx = 0; idx < threadNumber; idx++) {
			try {
				threads[idx].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		long endTime = System.nanoTime();
		this.totalTimes = endTime - startTime;
	}
}
